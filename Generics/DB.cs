﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class DB
    {
        private Dictionary<Guid, object> db = new Dictionary<Guid, object>();

        public T Add<T>()
            where T : class, new()
        {
            var obj = new T();
            var guid = Guid.NewGuid();
            db.Add(guid, obj);
            return obj;
        }

        public List<KeyValuePair<Guid, T>> GetAll<T>()
            where T : class, new()
        {
            return db.Where(x => x.Value.GetType().Equals(typeof(T)))
                .Select(y => new KeyValuePair<Guid, T>(y.Key, (T)y.Value)).ToList();
        }

        public T GetValue<T>(Guid guid)
            where T : class, new()
        {
            try
            {
                return (T)db[guid];
            }
            catch
            {
                return null;
            }
        }
    }
}
