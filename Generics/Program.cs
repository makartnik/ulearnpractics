﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class Program
    {
        static void Main(string[] args)
        {
            var p = Processor.CreateEngine<int>().With<string>().For<double>();
            var db = new DB();
            db.Add<Random>();
            db.Add<DB>();
            db.Add<Random>();
            var a = db.GetAll<Random>();
            foreach (var b in a)
                Console.WriteLine("{0} ", b);
            var c = db.GetValue<DB>(a[0].Key);
            Console.WriteLine(c == null);
            var d = db.GetValue<Random>(a[0].Key);
            Console.WriteLine(d);

        }
    }
}
