﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class ProcessorBuilder<TEngine>
    {
        public ProcessorBuilder<TEngine,TEntity> With<TEntity>()
        {
            return new ProcessorBuilder<TEngine, TEntity>();
        }
    }

    class ProcessorBuilder<TEngine, TEntity>
    {
        public Processor<TEngine, TEntity, TLogger> For<TLogger>()
        {
            return new Processor<TEngine, TEntity, TLogger>();
        }
    }
}
