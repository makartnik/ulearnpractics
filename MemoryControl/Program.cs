﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Drawing;

namespace MemoryControl
{
    class Program
    {
        static void Main(string[] args)
        {
            var timer = new Timer();
            using (timer.Start())
            {
                Thread.Sleep(100);
            }
            Console.WriteLine(timer.ElapsedMilliseconds);
            Thread.Sleep(100);
            using (timer.Continue())
            {
                Thread.Sleep(100);
            }
            Console.WriteLine(timer.ElapsedMilliseconds);
            var bitmap = new Bitmap("in.png");
            using (timer.Restart())
            {
                using (var editor = new BitmapEditor(bitmap))
                {
                    for (int x = 0; x < bitmap.Width; ++x)
                        for (int y = 0; y < bitmap.Height; ++y)
                            editor.SetPixel(x, y, 0, 0, 255);
                }
            }
            Console.WriteLine(timer.ElapsedMilliseconds);
            bitmap.Save("out1.png", System.Drawing.Imaging.ImageFormat.Png);
            bitmap = new Bitmap("in.png");
            Color color = Color.FromArgb(0, 0, 255);
            using (timer.Restart())
            {
                for (int x = 0; x < bitmap.Width; ++x)
                    for (int y = 0; y < bitmap.Height; ++y)
                        bitmap.SetPixel(x, y, color);
            }
            Console.WriteLine(timer.ElapsedMilliseconds);
            bitmap.Save("out2.png", System.Drawing.Imaging.ImageFormat.Png);

        }
    }
}
