﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace MemoryControl
{
    class Timer : IDisposable
    {
        Stopwatch timer = new Stopwatch();
        public long ElapsedMilliseconds { get { return timer.ElapsedMilliseconds; } }

        public void Dispose()
        {
            timer.Stop();
        }

        public Timer Continue()
        {
            timer.Start();
            return this;
        }

        public Timer Start()
        {
            timer.Start();
            return this;
        }

        public Timer Restart()
        {
            timer.Restart();
            return this;
        }

        public Timer Stop()
        {
            timer.Stop();
            return this;
        }
    }
}
