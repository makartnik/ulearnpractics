﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace MemoryControl
{
    class BitmapEditor : IDisposable
    {
        private Bitmap _bitmap;
        BitmapData data;

        public BitmapEditor(Bitmap bitmap)
        {
            _bitmap = bitmap;
            data = _bitmap.LockBits(new Rectangle(0, 0, _bitmap.Width, _bitmap.Height),
                ImageLockMode.ReadWrite, _bitmap.PixelFormat);
        }

        public void Dispose()
        {
            _bitmap.UnlockBits(data);
        }

        public void SetPixel(int x, int y, byte r, byte g, byte b)
        {
            byte[] rgb = { b, g, r };
            System.Runtime.InteropServices.Marshal.Copy(rgb, 0, data.Scan0 + data.Stride * y + x * 3, rgb.Length);
        }
    }
}
